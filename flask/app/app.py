from flask import Flask, request
import logging

logging.basicConfig(level=logging.DEBUG)
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/post/endpoint', methods=['POST'])
def show_post():
    app.logger.info(request.data)
    return 'ok'

if __name__ == '__main__':
    app.run(debug=True, port=8080, host='0.0.0.0')