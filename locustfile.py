from locust import HttpLocust, TaskSet, task, between


class UserBehavior(TaskSet):
    @task()
    def index(self):
        self.client.get("/")

    @task()
    def post_page(self):
        payload = {"some": "payload"}
        response = self.client.post("/post/endpoint", json=payload)
        # print("Response status code:", response.status_code)
        # print("Response content:", response.text)


class WebsiteUser(HttpLocust):
    task_set = UserBehavior

    # wait between 5.0 and 9.0 seconds after each task
    # wait_time = between(5, 9)
    wait_time = between(1, 2)
