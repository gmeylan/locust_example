# Locust

## How to install Locust

### Unix systems
`$ pip3 install locust`

### macOs

The following is currently the shortest path to installing gevent on OS X using Homebrew.

1) Install Homebrew.
2) Install libev (dependency for gevent): `brew install libev`

## How to use Locust

You can use Locust either with a container or locally on your machine. There's an example Dockerfile in this repository to use Locust

### Locally

You will find some examples in the file called `run_locust.sh`.

### Container

`LOCUST_MODE`
One of ‘standalone’, ‘master’, or ‘slave’. Defaults to ‘standalone’.

`LOCUSTFILE_PATH`
The path inside the container to the locustfile. Defaults to ‘/locustfile.py’

`LOCUST_MASTER_HOST`
The hostname of the master.

`LOCUST_MASTER_PORT`
The port used to communicate with the master. Defaults to 5557.

`LOCUST_OPTS`
Additional options to pass to locust. Defaults to ‘’

Examples:
`docker run -p 8089:8089 --volume $PWD/dir/of/locustfile:/mnt/locust -e LOCUSTFILE_PATH=/mnt/locust/locustfile.py -e TARGET_URL=https://abc.com locustio/locust`

## Test environment

In this repository you'll find a test environment for locust. It's self contained. You only need `docker` and `docker-compose` to run it. It will run a Locust instance with a Flask project to run the API to target. There's a get and post example in the locust. The network has already been set up to work correctly. Reminder: localhost inside the locust container will not target your machine but inside the container
